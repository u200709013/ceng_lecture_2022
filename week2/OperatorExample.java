public class OperatorExample {
	
	public static void main(String[] args){

		double value = 9D / 2;

		System.out.println(value);

		int a = 9 + (2 * 3);

		System.out.println(a);

		System.out.println(Integer.MAX_VALUE);
		System.out.println(Math.PI);

		double d = Math.pow(2,3);
		System.out.println(d);

		int b = 4;
		int c = b;

		b = b +1;

		System.out.println(hypothenus(3,4));
		System.out.println(hypothenus(12,5));

	}


	public static double hypothenus(double edge1, double edge2){

		double sumsqs = Math.pow(edge1,2) + Math.pow(edge2,2);

		return Math.sqrt(sumsqs);
	}
	
}