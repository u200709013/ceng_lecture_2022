public class ArrayExamples2 {
	
	public static void main(String[] args){

		String[][] multiString =
				{{"A", "B","C"},{"D", "E"}};



		for(int i = 0; i< multiString.length; i++){
			for(int j = 0; j <multiString[i].length; j++){
				System.out.print(multiString[i][j]);
			}
			System.out.println();
		}


		int[] values = { 6,4,3,8,1};

		int max = values[0];

		for(int i=1; i< values.length; i++){
			if (max < values[i])
				max = values[i];
		}

		System.out.println(max);

	}
	
}