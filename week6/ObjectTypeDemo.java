public class ObjectTypeDemo {

    public static void main(String[] args){

        System.out.println(Circle.PI);
        //System.out.println(Circle.radius);
        double a = Math.PI;

        Point p1 = new Point(5,10);
        Point p2 = new Point(5,10);

        p2 = p1;

        if (p1 == p2)
            System.out.println("p1 and p2 are same objects");
        else
            System.out.println("p1 and p2 are different objects");

        p1.xCoord = 15;

        System.out.println("p2.x = "  + p2.xCoord);


        Car car = new Car("Yellow");

        System.out.println("Car's color is " + car.color);


        Car car2 = new Car();
        car2.color = "Green";

        p1 = new Point(5,10);
        Circle circle = new Circle(5,p1);

        System.out.println("circle.r = " + circle.getRadius());
        System.out.println("circle.center.x = " + circle.getCenter().xCoord);
        System.out.println("circle.center.x = " + circle.getCenter().yCoord);
        p1.xCoord = 20;
        System.out.println("circle.center.x = " + circle.getCenter().xCoord);
        p1 = null;
        System.out.println("p1 = " + p1);

        System.out.println("circle.center.x = " + circle.getCenter().xCoord);

        circle.setCenter(p1);

        //System.out.println("circle.center.x = " + circle.center.xCoord);

        p2 = null;

        System.out.println(p1 == p2);

        //circle.PI = 3;


        Circle c2 = new Circle(7,new Point(4,8));
        c2.setRadius(3);
        //c2.PI = 3.1;




        System.out.println("circle.PI = " + circle.PI);
        System.out.println("c2.PI= " + c2.PI);

        System.out.println("# of point created = " + Point.count);
    }
}
